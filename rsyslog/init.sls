{% from "rsyslog/map.jinja" import rsyslog with context %}

rsyslog:
  pkg.installed:
    - names:
      - {{ rsyslog.package }}
      {%- if rsyslog.gnutls %}
      - {{ rsyslog.package_tls }}
      {%- endif %}
  {%- if rsyslog.managed %}
  file.managed:
    - name: {{ rsyslog.config }}
    - template: jinja
    - source: salt://rsyslog/templates/rsyslog.conf.jinja
    - context:
      config: {{ salt['pillar.get']('rsyslog', {}) }}
  {% endif %}
  service.running:
    - enable: True
    - name: {{ rsyslog.service }}
    - require:
      - pkg: {{ rsyslog.package }}
    {%- if rsyslog.managed %}
    - watch:
      - file: {{ rsyslog.config }}
    {% endif %}

{%- if rsyslog.managed %}
workdirectory:
  file.directory:
    - name: {{ rsyslog.workdirectory }}
    - user: {{ rsyslog.runuser }}
    - group: {{ rsyslog.rungroup }}
    - mode: 755
    - makedirs: True
{% endif %}

{%- for newfile, properties in rsyslog.custom.iteritems() %}
{%- if properties and properties.get('source') %}
{% set filename = properties.source %}
{% set basename = filename.split('/')|last %}
{% else %}
{% set filename = newfile %}
{% set basename = newfile %}
{% endif %}
rsyslog_custom_{{basename}}:
  file.managed:
    - name: {{ rsyslog.custom_config_path }}/{{ basename|replace(".jinja", "") }}
    {% if properties and properties.get('source') %}
    {%- if basename != filename %}
    - source: {{ filename }}
    {%- else %}
    - source: salt://rsyslog/files/{{ filename }}
    {%- endif %}
    {%- if filename.endswith('.jinja') %}
    - template: jinja
    {%- endif %}
    {%- else %}
    - contents: |
        {{ properties.get('content') | indent(8) }}
    {%- endif %}
    - watch_in:
      - service: {{ rsyslog.service }}
{%- endfor %}
