rsyslog:
  managed: True
  gnutls: True
  imjournal: True
  custom:
    20-audit.conf:
      content: |
        $PreserveFQDN on
        $DefaultNetstreamDriverCAFile /srv/certs/bundleca.pem
        module(load="imfile" mode="inotify")
        input(type="imfile" File="/var/log/audit/audit.log" Tag="audit" Severity="info" Facility="local0")
        if $programname == "audit" then {
          *.* action(type="omfwd" Template="RSYSLOG_SyslogProtocol23Format" Protocol="tcp" Target="log-01.rz02.deposit-solutions.com" Port="13002" StreamDriverMode="1" StreamDriver="gtls" StreamDriverAuthMode="anon" queue.type="LinkedList" queue.filename="audit_fwd" action.resumeRetryCount="-1" queue.saveonshutdown="on")
          stop
        }
    50-default.conf:
      source: 50-default.conf
